#With min heap
from random import randrange
class Node:
    def __init__(self, index, priority):
        self.index = index
        self.priority = priority
class PriorityQueue:

    def __init__(self, size):
        self.heapSize = size
        self.heap = []
   # def __init__(self, array):
    #    for i in array:
     #       self.add(i)

    #Check isEmpty
    def isEmpty(self):
        return self.heapSize == 0

    #Clear
    def clear(self):
        del self.heap[:]
        self.heapSize = 0

    def peek(self):
        if self.isEmpty():
            return None

        return self.heap[0]

    def poll(self):
        return self.removeAt(0)

    def contains(self, data):
        for i in self.heap:
            if i == data:
                return True

        return False

    def add(self, data):
        if data is None:
            return

        self.heap.append(data)
        self.swim(self.heapSize)
        self.heapSize += 1

    #Return true if heap[index1] < heap[index2]
    def compareLess(self, index1, index2):
        if self.heap[int(index1)] is None:
            return True
        if self.heap[int(index2)] is None:
            return False
        return self.heap[int(index1)].priority <= self.heap[int(index2)].priority

    def swim(self, index):
        parent = (index - 1) / 2

        #Check if k < parent swap data, when finish lower node will go up (floating bubble)
        while index > 0 and self.compareLess(index, parent):
            #Swap
            self.swap(parent, index)

            #Go to next parent
            index = parent
            parent = (index - 1) / 2

    #Check and swim
    def sink(self, index):
        while 1:
            #get node left of node index
            left = 2 * index + 1

            #get node right of node index
            right = 2 * index + 2
            smallest = left

            if right < self.heapSize and self.compareLess(right, left):
                smallest = right

            if left >= self.heapSize or self.compareLess(index, smallest):
                break

            self.swap(smallest, index)
            index = smallest

    def updatePriority(self, index, priority):
        for i in range(self.heapSize):
            if self.heap[i].index == index:
                self.heap[i].priority = priority
                self.swim(i)
                return

    def swap(self, index1, index2):
        index1 = int(index1)
        index2 = int(index2)
        data1 = self.heap[index1]
        data2 = self.heap[index2]

        self.heap[index1] = data2
        self.heap[index2] = data1

    def remove(self, data):
        if data is None:
            return

        for i in range(self.heapSize):
            if self.heap[i] == data:
                self.removeAt(i)
                return

        print ("Can't find this data")
        return

    def removeAt(self, index):
        if self.isEmpty():
            return

        self.heapSize -= 1
        remove_data = self.heap[index]
        self.swap(index, self.heapSize)
        self.heap[self.heapSize] = None

        #Check if it is last node was add
        if index == self.heapSize:
            return remove_data
        data = self.heap[index]

        self.sink(index)

        if self.heap[index] == data:
            self.swim(index)
        return remove_data

    def toString(self):
        returnStr = ""
        for i in self.heap:
            if i is not None:
                returnStr += ", " + str(i.index)
            else:
                returnStr += ", None"
        return returnStr
