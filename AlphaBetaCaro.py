from random import randrange
from graphics import *
class Move:
    def __init__(self, moveIndex, score):
        self.moveIndex = moveIndex
        self.score = score

class AlphaBetaEvaluation:
    currentState = None
    ply = 2
    state = None

    def __init__(self, ply):
        self.ply = ply

    def bestMove(self, state, player, opponent):
        self.state = state.copy()
        print (self.ply)
        move = self.alphaBeta(self.state, self.ply, player, opponent, -9999, 9999)
        #Check if move is None (table is full)
        if move.moveIndex is None:
            return -1
        return move.moveIndex

    def alphaBeta(self, state, ply, player, opponent, alpha, beta):
        #Get list index player can tick
        listMove = player.validMove(state)

        #If is last ply or dont have available move
        if ply == 0 or len(listMove) == 0 or state.checkWin() != "none":
            #Return score
            return Move(None, player.score(state))

        #Instianate best move
        best = Move(None, alpha)
        #Iterator in listMove, get move have max score
        for i in listMove:
            #Player tag is 'x' or 'o', use to calculate score
            #Set current move to 'x' or 'o'
            state.board[i] = player.tag

            #Calculate score in next-next.... move
            me = self.alphaBeta(state, ply - 1 , opponent, player, -beta, -alpha)

            #Restore to old state
            state.board[i] = '_'

            #Alpha-beta algorithm
            if -me.score > alpha:
                alpha = -me.score
                best = Move(int(i), alpha)
            if alpha >= beta:
                return best
        return best

class Game:
    board = ["_"] * 9
    win_combine = [
       [0, 1, 2],
       [3, 4, 5],
       [6, 7, 8],
       [0, 3, 6],
       [1, 4, 7],
       [2, 5, 8],
       [0, 4, 8],
       [2, 4, 6],
    ]
    win_list = []

    #Return avalable move
    def returnMoveAvailable(self):
        availableList = []
        for i in range(9):
            if self.board[i] == "_":
                availableList.append(i)

        return availableList

    #Return sum x when input is sum ord of 1 row|column|diagonal
    def returnSumX(self, sumORD):
        sum = 0
        if sumORD == ord('x')*2 + ord('_'):
            sum += 10
        elif sumORD == ord('x') + 2*ord('_'):
            sum += 1
        elif sumORD == ord('o')*2 + ord('_'):
            sum -= 10
        elif sumORD == ord('o') + 2*ord('_'):
            sum -= 1
        return sum


    #Caculate evaluation of X
    def calculateX(self):
        sum = 0
        winResult = self.checkWin()
        if winResult == 'x':
            sum += 100
            return sum
        if winResult == 'o':
            sum -= 100
            return sum

        #Calculate row
        row = 0
        for i in range(3):
            sumORD = ord(self.board[row]) + ord(self.board[row+1]) + ord(self.board[row+2])
            sum += self.returnSumX(sumORD)
            row += 3

        #Calculate column
        for i in range(3):
            sumORD = ord(self.board[i]) + ord(self.board[i+3]) + ord(self.board[i+6])
            sum += self.returnSumX(sumORD)

        #Calculate 2 diagonals
        sumORDDiagonals1 = ord(self.board[0]) + ord(self.board[4]) + ord(self.board[8])
        sumORDDiagonals2 = ord(self.board[2]) + ord(self.board[4]) + ord(self.board[6])
        sum += self.returnSumX(sumORDDiagonals1) + self.returnSumX(sumORDDiagonals2)

        return sum

    #Sum of x and o = 0
    def calculateO(self):
        return -self.calculateX()


    def checkWin(self):
        for condition in self.win_combine:
            if self.board[condition[0]] == self.board[condition[1]] == self.board[condition[2]]:
                if self.board[condition[0]] == 'x':
                    return 'x'
                elif self.board[condition[0]] == 'o':
                    return 'o'
        return "none"

    def pickColorWin(self):
        for condition in self.win_combine:
            if self.board[condition[0]] == self.board[condition[1]] == self.board[condition[2]]:
                if self.board[condition[0]] != '_':
                    self.win_list.append(condition[0])
                    self.win_list.append(condition[1])
                    self.win_list.append(condition[2])
                    return


    def isDraw(self):
        if len(self.returnMoveAvailable()) == 0 and self.checkWin() == "none":
            return True
        return False

    def copy(self):
        copyGame = Game()
        for i in range(9):
            copyGame.board.append(self.board[i])
        return copyGame

    def printPosition(self):
        returnStr = "--------------"
        for i in range(9):
            if i % 3 == 0:
                if i != 0:
                    returnStr += " |\n"
                else:
                    returnStr += "\n"
            if self.board[i] == '_':
                returnStr += " | " + str(i)
            else:
                returnStr += " | "
                if i in self.win_list:
                    returnStr += '\x1b[6;30;41m' + self.board[i] + '\x1b[0m'
                else:
                    if self.board[i] == 'o':
                        returnStr += '\x1b[1;31;40m' + self.board[i] + '\x1b[0m'
                    else:
                        returnStr += '\x1b[1;32;40m' + self.board[i] + '\x1b[0m'
        returnStr += " |\n--------------"
        return returnStr

    def play(self):
        player = Player('x')
        computer = Player('o')

        win = GraphWin('Tic Tac Toe', 500, 500)
        win.setBackground('yellow')
        y = 100
        x = 100
        for i in range(4):
            line = Line(Point(100,y),Point(400,y))
            line2 = Line(Point(x,100),Point(x, 400))
            y += 100
            x += 100
            line.draw(win)
            line2.draw(win)
        while 1:
            isTick = False
            point = Point(0,0)
            pos = win.getMouse()
            choose = -1

            tempX = 100
            for i in range(1,4):
                if pos.x > tempX* i and pos.x < tempX * (i+1) and pos.y > 100 and pos.y < 200:
                    choose = i - 1
                    if self.board[choose] != '_':
                        print("Already have a node here")
                        continue
                    else:
                        isTick = True
                        point = Point(tempX*i + 50,150)

            tempX = 100
            for i in range(1,4):
                if pos.x > tempX * i and pos.x < tempX *(i +1) and pos.y > 200 and pos.y < 300:
                    choose = i - 1 + 3
                    if self.board[choose] != '_':
                        print("Already have a node here")
                        continue
                    else:
                        isTick = True
                        point = Point(tempX*i + 50,250)

            tempX = 100
            for i in range(1,4):
                if pos.x > tempX * i and pos.x < tempX *(i +1) and pos.y > 300 and pos.y < 400:
                    choose = i - 1 + 6
                    if self.board[choose] != '_':
                        print("Already have a node here")
                        continue
                    else:
                        isTick = True
                        point = Point(tempX*i + 50,350)

            if isTick:
                self.board[choose] = 'x'
                labelX = Text(point, 'x')
                labelX.setTextColor('green')
                labelX.setSize(20)
                labelX.draw(win)


                abEval = AlphaBetaEvaluation(8)
                moveAI = abEval.bestMove(self, computer, player)
                del abEval

                print("AI MOVE: " + str(moveAI))

                pointDrawO = Point(0,0)
                self.board[moveAI] = 'o'
                if moveAI < 3:
                    pointDrawO = Point(150 + 100*moveAI, 150)
                elif moveAI >= 3 and moveAI < 6:
                    pointDrawO = Point(150 + 100*(moveAI - 3),250)
                elif moveAI >= 6 and moveAI < 9:
                    pointDrawO = Point(150 + 100*(moveAI - 6),350)
                labelO = Text(pointDrawO, 'o')
                labelO.setTextColor('red')
                labelO.setSize(20)
                labelO.draw(win)

            if self.isDraw():
                print("Draw!")
                context = Text(Point(250,50),"Draw!!")
                context.setTextColor('red')
                context.setSize(20)
                context.draw(win)
                break

            isWin = self.checkWin()
            if isWin == "none":
                continue
            elif isWin == 'o':
                print("Computer Win")
                context = Text(Point(250,50),"Computer win")
                context.setTextColor('red')
                context.setSize(20)
                context.draw(win)
                break
            else:
                print("You win") #Just kidding, you will never win with ply = 8
                context = Text(Point(250,50),"You win")
                context.setTextColor('red')
                context.setSize(20)
                context.draw(win)
                break


        # while (1):

        #     print("---------------------------------")
        #     print("Current game")
        #     print(self.printPosition())
        #     print("Choose your position: (0-8)")
        #     choose = int(input())
        #     if choose < 0 or choose > 8:
        #         print ("Wrong position")
        #         continue
        #     else:
        #         if self.board[choose] != '_':
        #             print("Already have a node here")
        #             continue

        #     self.board[choose] = 'x'

        #     abEval = AlphaBetaEvaluation(8)
        #     moveAI = abEval.bestMove(self, computer, player)
        #     print("AI MOVE: " + str(moveAI))

        #     self.board[moveAI] = 'o'

        #     if self.isDraw():
        #         print("Draw!")
        #         print(self.printPosition())
        #         break

        #     isWin = self.checkWin()
        #     if isWin == "none":
        #         continue
        #     elif isWin == 'o':
        #         print("Computer Win")
        #         self.pickColorWin()
        #         print(self.printPosition())
        #         break
        #     else:
        #         print("You win") #Just kidding, you will never win with ply = 8
        #         self.pickColorWin()
        #         print(self.printPosition())
        #         break


class Player:
    tag = 'x'

    def __init__(self, tag):
        self.tag = tag

    def score(self, game):
        if self.tag == 'x':
            return int(game.calculateX())
        else:
            return int(game.calculateO())

    def validMove(self, game):
        return game.returnMoveAvailable()

def testAlphaBeta():
    #ascii_banner = pyfiglet.figlet_format("Tic tac toe")
    #print(ascii_banner)
    print('\033[1m \033[91m \033[4m' '\x1b[1;32;40m' + 'TIC TAC TOE'  + '\x1b[0m' + '\033[0m')
    game = Game()
    game.play()
