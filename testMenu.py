from Dijkstra import *
from DijkstraDenseGraph import *
from AlphaBetaCaro import *
from EightPuzzle import *
def mainTest():
    while 1:
        print("-------------")
        print("Choose option: ")
        print("0. Exit.")
        print("1. Test Dijkstra.")
        print("2. Test Dijkstra Dense Graph")
        print("3. Test AlphaBeta with Tic-tac-toe")
        print("4. Test A* with 8-Puzzle")

        option = int(input())

        if option == 0:
            break
        elif option == 1:
            testDijkstra()
        elif option == 2:
            testDenseGraph()
        elif option == 3:
            testAlphaBeta()
        elif option == 4:
            testPuzzle()

mainTest()
