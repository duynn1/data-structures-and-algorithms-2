import heapq 
import time
from random import randrange
from graphics import *
import os 
class Move:
    #Direction : 1 - Up , 2 - Down, 3 - Left, 4 - Right
    def __init__(self, index, direction, number = -1):
        self.index = index
        self.direction = direction
        self.number = number

class Game:
    def __lt__(self,other):
        return self.f < other.f

    def countID(self):
        j = 1
        for i in range(9):
            self.id += self.table[i] * j
            j *= 10

    def __init__(self):
        self.trueState = [1,2,3,8,0,4,7,6,5]
        #self.table = [1,8,2,0,4,3,7,6,5]
        #self.table = [0,1,3,8,2,4,7,6,5]
        # self.table = [8,1,2,0,4,3,7,6,5]
        # self.table = [8,1,3,0,4,5,2,7,6]
        #self.table = [2,8,3,1,6,4,7,0,5]
        self.table = [2,8,1,0,4,3,7,6,5]
        self.clockwise = [[0,1],[1,2],[2,5],[5,8],[8,7],[7,6],[6,3],[3,4]]
        self.id = 0
        self.move = []
        self.depth = 0
        self.f = 0 #f = score + depth
        self.score = 0
        self.parent = None
        self.countID()

    #Calculate score of table
    def calculateScore(self):
        score = 0
        score2 = 0

        #Check clockWise
        for pair in self.clockwise:
            number = self.table[pair[0]]
            number1 = self.table[pair[1]]

            #Check wrong by clockwise, except 8 - 0
            if number + 1 != number1 and abs(number1 - number)!= 8:
                score += 2

        #Check distance
        for i in range(9):
            if self.table[i] == 0:
                continue
            trueIndex = self.returnTrueIndex(self.table[i])
            distance = self.distanceFromTrueIndex(i,trueIndex)
            score2 += distance

        #Check center
        if self.table[4] != 0:
            score +=1

        score *= 3
        score += score2
        self.score = score
        return score

    #Return true index of a number
    def returnTrueIndex(self, number):
        for i in range(9):
            if self.trueState[i] == number:
                return i

    #Calculate distance from index in table to index in true table
    def distanceFromTrueIndex(self, tableIndex, trueIndex):
        if tableIndex == trueIndex:
            return 0
        if tableIndex - 3 == trueIndex or tableIndex + 3 == trueIndex or \
           tableIndex - 1 == trueIndex or tableIndex + 1 == trueIndex:
            return 1

        if tableIndex + 2 == trueIndex or tableIndex - 2 == trueIndex or \
           tableIndex + 4 == trueIndex or tableIndex - 4 == trueIndex:
            return 2

        if tableIndex == 6 and trueIndex == 2 or \
           tableIndex == 2 and trueIndex == 6 or \
           tableIndex == 0 and trueIndex == 8 or \
           tableIndex == 8 and trueIndex == 0:
            return 4

        return 3

    #Return list move availabel
    def availableMove(self, table):
        listMove = []
        indexOf0 = -1
        #Get index of number 0
        for i in range(9):
            if table[i] == 0:
                indexOf0 = i
                break

        print("index of 0: {}".format(indexOf0))
        #Check have up? -> add Move to down
        upIndex = indexOf0 - 3
        if upIndex >= 0 and upIndex <= 8:
            listMove.append(Move(upIndex,2,table[upIndex]))

        #Check have down? -> add Move to up
        downIndex = indexOf0 + 3
        if downIndex >= 0 and downIndex <= 8:
            listMove.append(Move(downIndex,1,table[downIndex]))

        #Check have left? -> add Move to right
        leftIndex = indexOf0 - 1
        temp1 = int(leftIndex / 3)
        temp2 = int(indexOf0 / 3)
        print("temp1 {} temp2 {}".format(temp1, temp2))
        if leftIndex >= 0 and leftIndex <= 8 and temp1 == temp2:
            print("right")
            listMove.append(Move(leftIndex,4,table[leftIndex]))

        #Check have left? -> add Move to left
        rightIndex = indexOf0 + 1
        temp3 = int(rightIndex / 3)
        if rightIndex >= 0 and rightIndex <= 8 and temp3 == temp2:
            print("left")
            listMove.append(Move(rightIndex,3,table[rightIndex]))

        return listMove

    def applyMove(self, move):
        data = self.table[move.index]
        self.table[move.index] = 0
        if move.direction == 1:
            self.table[move.index - 3] = data
        elif move.direction == 2:
            self.table[move.index + 3] = data
        elif move.direction == 3:
            self.table[move.index - 1] = data
        elif move.direction == 4:
            self.table[move.index + 1] = data

    def printTable(self):
        returnStr = ""
        for i in range(9):
            if i % 3 == 0:
                returnStr += "\n"

            if self.table[i] == 0:
                returnStr += " -"
            else:
                returnStr += " " + str(self.table[i])

        return returnStr

    def compare(self, goal):
        for i in range(9):
            if self.table[i] != goal.table[i]:
                return False
        return True

    #Copy a game
    def copy(self):
        newGame = Game()
        for i in range(9):
            newGame.table[i] = self.table[i]
        newGame.depth = self.depth
        newGame.score = self.score
        newGame.parent = self.parent
        return newGame


def isContain(listGame, item):
    for game in listGame:
        if type(game) is tuple:
            #if np.array_equal(game[1].table,item.table):
            if game[1].id == item.id:
                return game
        else:
            #if np.array_equal(game.table,item.table):
            if game.id == item.id:
                return game

    return None


def getMove(start, goal):
    start0Index = -1
    goal0Index = -1
    for i in range(9):
        if start.table[i] == 0:
            start0Index = i
        if goal.table[i] == 0:
            goal0Index = i

    delta = goal0Index - start0Index
    print("delta {}".format(delta))
    if delta == 1:
        move =  Move(start0Index + 1,3)
        move.number = start.table[start0Index + 1]
        return move
    if delta == -1:
        move =  Move(start0Index - 1,4)
        move.number = start.table[start0Index - 1]
        return move
    if delta == 3:
        move =  Move(start0Index + 3,1)
        move.number = start.table[start0Index + 3]
        return move
    if delta == -3:
        move =  Move(start0Index - 3,2)
        move.number = start.table[start0Index - 3]
        return move

def search(initial, goal):
    open = []
    copyGame = initial.copy()
    score = copyGame.calculateScore()
    copyGame.f = score

    heapq.heappush(open, (copyGame.f, copyGame))

    #open.append(copyGame)
    listMove = []
    closed = []
    depth = 0
    best = None
    while len(open) != 0:
        best = heapq.heappop(open)[1]
        best.calculateScore()
        if best.score == 0:
            print("break")
            break;

        closed.append(best)

        listAvailabelMove = best.availableMove(best.table)
        for move in listAvailabelMove:
            copy = best.copy()
            copy.applyMove(move)
            copy.depth = best.depth + 1
            copy.calculateScore()
            copy.countID()

            if isContain(closed,copy) is not None:
                continue

            exist = isContain(open,copy)

            if exist is None:
                copy.f = copy.depth + copy.score
                copy.parent = best
                #print(copy.f)
                heapq.heappush(open,(copy.f,copy))
            else:
                if depth + 1 + exist[1].score < exist[1].f:
                    newEntry = exist[1].copy()
                    newEntry.depth = depth
                    newEntry.f = depth + exist[1].score
                    newEntry.parent = best
                    open.remove(exist)
                    heapq.heapify(open)
                    heapq.heappush(open,(newEntry.f,newEntry))
    path = []
    while best != None:
        path.append(best)
        best = best.parent

    listMove = []
    path.reverse()
    for i in path:
        print("----")
        print(i.printTable())
    for j in range (len(path) - 1):
        move = getMove(path[j],path[j+1])
        print("Move index {} to {}".format(move.index,move.direction))
        listMove.append(move)
    return listMove


def testPuzzle():
    game = Game()
    goal = Game()
    goal.table = [1,2,3,8,0,4,7,6,5]

    # table1 = [2,8,1,0,4,3,7,6,5]
    # table2 = [8,1,2,0,4,3,7,6,5]
    # table3 = [8,1,3,0,4,5,2,7,6]
    # table4 = [2,8,3,1,6,4,7,0,5]
    # table5 = [8,3,5,4,1,6,2,7,0]
    # listTable = []
    # listTable.append(table1)
    # listTable.append(table2)
    # listTable.append(table3)
    # listTable.append(table4)
    # listTable.append(table5)
    # rantable = randrange(5)
    # game.table = listTable[rantable]

    win = GraphWin('8 - Puzzle', 500, 500)
    #Draw restart Button



    win.setBackground('yellow')
    y = 100
    x = 100
    for i in range(4):
        line = Line(Point(100,y),Point(400,y))
        line2 = Line(Point(x,100),Point(x, 400))
        y += 100
        x += 100
        line.draw(win)
        line2.draw(win)
    listRect = []
    listText = []
    index = 0

    game.table = [1,2,3,8,0,4,7,6,5]

    for j in range(1,4):
        for i in range(1,4):
            rect = Rectangle(Point(100*i,100*j),Point(100*(i+1),100*(j+1)))
            rect.setFill("blue")
            listRect.append(rect)
            label = Text(Point(100*i + 50,100*j + 50),str(game.table[index]))
            label.setTextColor('white')
            label.setSize(20)
            listText.append(label)

            if game.table[index] != 0:
                rect.draw(win)
                label.draw(win)
            index += 1


    startRandomPuzzle(win,game,listText, listRect)



    while 1:
        pos = win.getMouse()
        if pos.x > 25 and pos.x < 200 and pos.y > 25 and pos.y < 50:
            os.system('python3 EightPuzzle.py')
            return

        if pos.x > 25 and pos.x < 200 and pos.y > 60 and pos.y < 90:
            startSolve(win,game,goal,listText, listRect)

def startSolve(win, game, goal, listText, listRect):
    listMove = search(game,goal)
    for move in listMove:
        number = move.number
        print(number)
        index = -1
        for i in range(9):
            if int(listText[i].getText()) == number:
                index = i
                break

        if move.direction == 1:
            print("move up in index {}".format(move.index))
            listText[index].move(0,-100)
            listRect[index].move(0,-100)
        if move.direction == 2:
            print("move down index {}".format(move.index))
            listText[index].move(0,100)
            listRect[index].move(0,100)
        if move.direction == 3:
            print("move left in index {}".format(move.index))
            listText[index].move(-100,0)
            listRect[index].move(-100,0)
        if move.direction == 4:
            print("move right in index {}".format(move.index))
            listText[index].move(100,0)
            listRect[index].move(100,0)
        time.sleep(0.2)


def startRandomPuzzle(win, game,listText, listRect):
    listStartMove = []
    for i in range(200):
        listAvailableMove = game.availableMove(game.table)
        ran = randrange(len(listAvailableMove))
        move = listAvailableMove[ran]
        game.applyMove(move)
        listStartMove.append(move)

    for move in listStartMove:
        number = move.number
        print(number)
        index = -1
        for i in range(9):
            if int(listText[i].getText()) == number:
                index = i
                break

        if move.direction == 1:
            print("move up in index {}".format(move.index))
            listText[index].move(0,-100)
            listRect[index].move(0,-100)
        if move.direction == 2:
            print("move down index {}".format(move.index))
            listText[index].move(0,100)
            listRect[index].move(0,100)
        if move.direction == 3:
            print("move left in index {}".format(move.index))
            listText[index].move(-100,0)
            listRect[index].move(-100,0)
        if move.direction == 4:
            print("move right in index {}".format(move.index))
            listText[index].move(100,0)
            listRect[index].move(100,0)

    restartButton = Rectangle(Point(25, 25), Point(200, 50))
    restartButton.setFill("Green")
    restartText = Text(Point(100,35),"Restart random puzzle")
    restartText.setTextColor('white')
    restartButton.draw(win)
    restartText.draw(win)

    startButton = Rectangle(Point(25, 60), Point(200, 90))
    startButton.setFill("Green")
    startText = Text(Point(100,80),"Start solve")
    startText.setTextColor('white')
    startButton.draw(win)
    startText.draw(win)
if __name__ == "__main__":
    testPuzzle()
