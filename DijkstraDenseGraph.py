import sys
from random import randrange
from graphics import *
import sys
import math
class DijkstraDenseGraph:
    #Save distance from source vertex to vertex in index
    

    def __init__(self, size):
        self.__dist = [9999] * size
        self.__pred = [-1] * size
        self.__weight = [[9999]for i in range(size)]
        for i in range(size):
            for j in range(size):
                self.__weight[i].append(9999)
        self.__visited = [False] * size
        self.__size = size
        self.__weightReturn = 0


    #Add function
    def add(self, v1, v2, weight):
        if self.__weight[v1][v2] != 9999:
            print ("Already have this edge")
            return
        self.__weight[v1][v2] = weight

    #Return list of vertex from s(source) to d(destination)
    def singleSourceSortestDense(self, s, d):
        self.__dist[s] = 0
        self.__weightReturn = 0

        while 1:
            #current vertex index
            u = -1
            weight = 9999
            for i in range(self.__size):
                if self.__visited[i] == False and self.__dist[i] < weight:
                    weight = self.__dist[i]
                    u = i

            if u == -1:
                break

            self.__visited[u] = True

            for v in range(self.__size):
                #get all neighboor of vertex u
                w = self.__weight[u][v]
                if v == u:
                    continue
                newLen = self.__dist[u] + w
                if newLen < self.__dist[v]:
                    self.__dist[v] = newLen
                    self.__pred[v] = u

        path  = []
        path.append(d)
        traver = self.__pred[d]
        self.__weightReturn += self.__dist[d]
        i = 0
        while traver != s:
            path.append(traver)
            traver = self.__pred[traver]
            self.__weightReturn += self.__dist[traver]
            i += 1
            if i > self.__size + 10:
                print ("Doesn't have connect")
                self.__weightReturn = 0
                self.reset()
                return None
        path.append(s)

        #If have a node have parent is -1 so dont have connect
        if -1 in path:
            print ("Doesn't have connect")
            self.__weightReturn = 0
            self.reset()
            return None

        path.reverse()
        self.reset()
        return path

    def reset(self):
        size = self.__size
        self.__dist = [9999] * size
        self.__pred = [-1] * size
        self.__visited = [False] * size

    def toString(self):
        strne = ""
        strne += str(self.__pred)
        strne += "\n" + str(self.__dist)
        return strne

    def getPred(self):
        return self.__pred

    def size(self):
        return self.__size
    def getListWeight(self):
        return self.__weight

    def returnWeightAfterSearch(self):
        return self.__weightReturn

def testDenseGraph():

    print("Enter number of verticles")
    num = int(input())
    while num < 0:
        print("Number must >= 2, input again")
        num = int(input())

    dijkstra = DijkstraDenseGraph(num)
    print ("Do you want auto generate graph? (y/n)")
    yes = input()
    if yes == "y":
        for i in range(num):
            for j in range(2):
                randWeight = randrange(15)
                randVerticle = randrange(num)
                if i == randVerticle:
                    continue

                print("Add verticle {} and verticle {} with weight = {}".format(i,randVerticle,randWeight))
                dijkstra.add(i,randVerticle,randWeight)


    win = GraphWin('Demo Dijkstra', 500, 500)
    listPoint = []
    win.setBackground('yellow')
    printGraph(win,dijkstra,listPoint)
    while(1):
        print("-----------------")
        print("Select option")
        print("0. Exit")
        print("1. Add edge (verticle1, verticles2, weight)")
        print("2. Find path from verticle1 to verticle2 (verticle1, verticle2)")
        print("3. Print graph")

        option = int(input())
        if option == 1:
            print("Enter index of Verticle1: ")
            v1 = int(input())
            print ("Enter index of Verticle2: ")
            v2 = int(input())
            print ("Enter weight: ")
            w = int(input())
            dijkstra.add(v1,v2,w)
            clear(win)
            printGraph(win,dijkstra,listPoint)
        elif option == 2:
            print ("Enter index of Verticle1: ")
            v1 = int(input())
            print ("Enter index of Verticle2: ")
            v2 = int(input())
            list = dijkstra.singleSourceSortestDense(v1,v2)
            print ("Sortest path from {} to {} with weight = {}".format(v1,v2, dijkstra.returnWeightAfterSearch()))
            print(list)
            if list is None:
                continue

            clear(win)
            printGraph(win,dijkstra,listPoint)
            drawPath(win,list,listPoint)
        elif option == 3:
            #Calculate delta = newValue - oldValue
            printGraph(dijkstra)
        else:
            break

def clear(win):
    for item in win.items[:]:
        item.undraw()
    win.update()

def drawPath(win, path, listPoint):
    for i in range(len(path)-1):
        line = Line(listPoint[path[i]],listPoint[path[i+1]])
        line.setOutline('red')
        line.setWidth(3)
        line.draw(win)

def printGraph(win,dijkstra,listPoint):

    label = Text(Point(150,25), '*This demo lack direction and weight maybe in same position\nif two node both connect with difference edge')
    label.setTextColor('red')
    label.setStyle('italic')
    label.setSize(12)
    label.draw(win)

    cir = Circle(Point(250,250), 200)
    #cir.draw(win)
    cir.setOutline('red')

    size = dijkstra.size()
    print(size)
    deltaAngle = 360/size
    currentAngle = 0

    listPoint.clear()
    for j in range(size):
        point = Point(250 + 200 * math.cos(currentAngle),250 + 200 * math.sin(currentAngle))
        label = Text(point, str(j))
        label.setTextColor('red')
        label.setStyle('italic')
        label.setSize(20)
        label.draw(win)
        point.draw(win)
        listPoint.append(point)
        currentAngle += deltaAngle

    listWeight = dijkstra.getListWeight()
    for k in range(size):
        for j in range(size):
            if listWeight[k][j] != 9999:
                point1 = listPoint[k]
                point2 = listPoint[j]
                line = Line(point1,point2)
                x = listPoint[j].x + listPoint[k].x + 10
                x /= 2
                y = listPoint[j].y + listPoint[k].y + 10
                y /= 2
                label = Text(Point(x,y), str(listWeight[k][j]))
                label.setTextColor('black')
                label.setStyle('italic')
                label.setSize(10)
                label.draw(win)
                line.draw(win)
