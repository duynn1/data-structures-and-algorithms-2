from PriorityQueue import *
from graphics import *
import sys
import math
#Use to save neighboor v of a vertex with weight(w)
class Entry:
    v = 0
    w = 0
    def __init__(self,v , w):
        self.v = v
        self.w = w


#Graph have a list contain index of vertex, in each index have a list neightboor
class Graph:
    #List vertex in graph
    #__listVertex = []
    #__size = 0

    def __init__(self, size):
        #Init with size
        self.__listVertex = [[]for i in range(size)]
        self.__size = size

    def size(self):
        return self.__size

    def add(self, v1, v2, weight):
        for i in self.__listVertex[v1]:
            if i is not None and i.v == v2:
                print ("Already have this edge")
                return
        
        self.__listVertex[v1].append(Entry(v2,weight))

    #Get neighboor of a vertex u
    def getList(self, u):
        return self.__listVertex[u]

    def getListToDraw(self):
        return self.__listVertex


class Dijkstra:
    #Save distance from source vertex to vertex in index
    #__dist = []
    #Save parent of vertex
    #__pred = []
    #

    def __init__(self, graph):
        self.__graph = graph
        self.__dist = [sys.maxsize] * graph.size()
        self.__pred = [-1] * graph.size()
        self.__pq = PriorityQueue(0)
        self.__weightReturn = 0

    #Return list of vertex from s(source) to d(destination)
    def findShortestWay(self, s, d):
        self.__dist[s] = 0

        #Add all node in graph to a Priority Queue with priority is distance from vertex s to that vertex
        self.__pq.clear()
        for i in range(self.__graph.size()):
            self.__pq.add(Node(i,self.__dist[i]))

        #Loop until priority queue empty
        while not self.__pq.isEmpty():
            u = self.__pq.poll().index

            #Check all neightboor of u
            #Each item in __graph.getList is an Entry(v,w)
            for ci in self.__graph.getList(u):
                #Get vertex index
                v = ci.v

                #newLen = parent len + weight
                newLen = self.__dist[u] + ci.w

                #if have a new path have smaller weight than old, update parent and priority
                if newLen < self.__dist[v]:
                    self.__pq.updatePriority(v, newLen)
                    self.__dist[v] = newLen
                    self.__pred[v] = u

        path  = []
        path.append(d)
        traver = self.__pred[d]
        self.__weightReturn += self.__dist[d]
        i = 0
        while traver != s:
            path.append(traver)
            traver = self.__pred[traver]
            self.__weightReturn += self.__dist[traver]
            if i > self.__graph.size() + 10:
                print ("Doesn't have connect 1")
                self.__weightReturn = 0
                return None
            i += 1
        path.append(s)

        #If have a node have parent is -1 so dont have connect
        if -1 in path:
            print ("Doesn't have connect 2")
            self.__weightReturn = 0
            return

        path.reverse()
        return path

    def returnWeightAfterSearch(self):
        return self.__weightReturn

    def toString(self):
        strne = ""
        strne += str(self.__pred)
        strne += "\n" + str(self.__dist)
        return strne

    def getPred(self):
        return self.__pred

def testDijkstra():

    print("Enter number of verticles")
    num = int(input())
    while num < 0:
        print("Number must >= 2, input again")
        num = int(input())

    graph = Graph(num)
    print ("Do you want auto generate graph? (y/n)")
    yes = input()
    if yes == "y":
        for i in range(num):
            for j in range(2):
                randWeight = randrange(1,15)
                randVerticle = randrange(num)
                if i == randVerticle:
                    continue

                print("Add verticle {} and verticle {} with weight = {}".format(i,randVerticle,randWeight))
                graph.add(i,randVerticle,randWeight)

    dijkstra = Dijkstra(graph)

    win = GraphWin('Demo Dijkstra', 500, 500)
    win.setBackground('yellow')

    label = Text(Point(150,25), '*This demo lack direction and weight maybe in same position\nif two node both connect with difference edge')
    label.setTextColor('red')
    label.setStyle('italic')
    label.setSize(12)
    label.draw(win)

    listPoint = []
    cir = Circle(Point(250,250), 200)
    #cir.draw(win)
    cir.setOutline('red')
    
    printGraph(win,graph,listPoint)
    while(1):
        print("-----------------")
        print("Select option")
        print("0. Exit")
        print("1. Add edge (verticle1, verticles2, weight)")
        print("2. Find path from verticle1 to verticle2 (verticle1, verticle2)")
        print("3. Print graph")

        option = int(input())
        if option == 1:
            print("Enter index of Verticle1: ")
            v1 = int(input())
            print ("Enter index of Verticle2: ")
            v2 = int(input())
            print ("Enter weight: ")
            w = int(input())
            graph.add(v1,v2,w)
            del dijkstra
            dijkstra = Dijkstra(graph)
            printGraph(win,graph,listPoint)
        elif option == 2:
            print ("Enter index of Verticle1: ")
            v1 = int(input())
            print ("Enter index of Verticle2: ")
            v2 = int(input())
            list = dijkstra.findShortestWay(v1,v2)
            if list is None:
                continue
            print ("Sortest path from {} to {} with weight = {}".format(v1,v2, dijkstra.returnWeightAfterSearch()))
            print(list)
            clear(win)
            printGraph(win,graph,listPoint)
            drawPath(win,graph, list, listPoint,)
            del dijkstra
            dijkstra = Dijkstra(graph)
        elif option == 3:
            #Calculate delta = newValue - oldValue
            printGraph(graph)
        else:
            break

def drawPath(win, graph, path, listPoint):
    for i in range(len(path)-1):
        line = Line(listPoint[path[i]],listPoint[path[i+1]])
        line.setOutline('red')
        line.setWidth(3)
        line.draw(win)

def clear(win):
    for item in win.items[:]:
        item.undraw()
    win.update()

def printGraph(win,graph,listPoint):

    listPoint.clear()
    listVertex = graph.getListToDraw()
    size = len(listVertex)

    deltaAngle = 360/size
    currentAngle = 0

    for j in range(size):
        point = Point(250 + 200 * math.cos(currentAngle),250 + 200 * math.sin(currentAngle))
        label = Text(point, str(j))
        label.setTextColor('red')
        label.setStyle('italic')
        label.setSize(20)
        label.draw(win)
        point.draw(win)
        listPoint.append(point)
        currentAngle += deltaAngle

    for k in range(size):
        for j in listVertex[k]:
            point1 = listPoint[k]
            point2 = listPoint[j.v]
            line = Line(point1,point2)
            x = listPoint[j.v].x + listPoint[k].x + 10
            x /= 2
            y = listPoint[j.v].y + listPoint[k].y + 10
            y/= 2
            label = Text(Point(x,y), str(j.w))
            label.setTextColor('black')
            label.setStyle('italic')
            label.setSize(10)
            label.draw(win)
            line.draw(win)
     #Get and draw three vertices of triangletaAngle

    #win.getMouse()
    #win.close()


